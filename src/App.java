public class App {
    public static void main(String[] args) throws Exception {
        System.out.println();
        Human man1 = new Human("Khongdet");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        System.out.println();

        Bird penguin = new Bird("Bird Penguin");
        penguin.eat();
        penguin.sleep();
        penguin.takeoff();
        penguin.walk();
        penguin.run();
        penguin.fly();
        penguin.landing();
        System.out.println();

        Plane helicopter = new Plane("Helicopter","Doraemon");
        helicopter.takeoff();
        helicopter.fly();
        helicopter.landing();
        System.out.println();

        Superman nobita = new Superman("Superman Nobita");
        nobita.eat();
        nobita.sleep();
        nobita.walk();
        nobita.run();
        nobita.takeoff();
        nobita.fly();
        nobita.landing();
        System.out.println();

        Submarine titanic = new Submarine("Titanic","Rose");
        titanic.swim();
        System.out.println();

        Fish Shark = new Fish("Shark");
        Shark.swim();
        Shark.eat();
        System.out.println();

        Cat Tom = new Cat("Tom");
        Tom.swim();
        Tom.eat();
        Tom.sleep();
        Tom.crawl();
        Tom.walk();
        Tom.run();
        System.out.println();

        Dog dog = new Dog("dog");
        dog.swim();
        dog.eat();
        dog.sleep();
        dog.crawl();
        dog.walk();
        dog.run();
        dog.crawl();
        System.out.println();

        Crocodile alligator = new Crocodile("Alligator");
        Rat jerry = new Rat("Jerry");
        Snake slytherin = new Snake("Slytherin");
        Bat batman = new Bat("Batman");


        Flyable[] flyables = {helicopter,penguin,nobita,batman};
        for (int i = 0; i < flyables.length; i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();

        Swimable[] swimables = {nobita,jerry,dog,alligator,Shark,man1,Tom};
        for (int i = 0; i < swimables.length; i++){
            swimables[i].swim();
        }
        System.out.println();

        Walkable[] walkables = {nobita,dog,jerry,alligator,man1,Tom,penguin};
        for (int i = 0; i < walkables.length; i++){
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println();
        Crawlable[] crawlables = {dog,alligator,Tom,jerry,slytherin};
        for (int i = 0; i < crawlables.length; i++){
            crawlables[i].crawl();
        }
    }
}
