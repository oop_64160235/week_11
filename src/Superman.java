public class Superman extends Human implements Flyable, Walkable, Swimable {
    public Superman(String name) {
        super(name);
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");

    }

    @Override
    public String toString() {
        return "Superman (" + getName() + ")";
    }

    @Override
    public void swim() {
        System.out.println(this + " Swim.");

    }

    @Override
    public void walk() {
        System.out.println(this + " walk.");

    }

    @Override
    public void run() {
        System.out.println(this + " run.");

    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");

    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");

    }

}
